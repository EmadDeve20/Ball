import pygame
import random

pygame.init()

frame_x = 500
frame_y = 500
frame = pygame.display.set_mode((frame_x,frame_y))
pygame.display.set_caption("Bull")
clock = pygame.time.Clock()
background = (250,250,250)

def Close():
    pygame.quit()
    quit()

def Bull(color,x = 100 , y = 100 , w = 0):
    bull =  pygame.draw.circle(frame , color , (x , y) , 50 ,w)
    frame.blit(frame , bull)

x = random.randrange(50,200)
y = random.randrange(50,200)
i = random.randrange(2)
if i == 0:
    y_change = -1
else:
    y_change = 1
i = random.randrange(2)
if i == 0:
    x_change = 1
else:
    x_change = -1

w = 0 ; w_change = 1
color = (random.randrange(256) , random.randrange(256) , random.randrange(256))
exiting = False
while not exiting:
    for eve in pygame.event.get():
        if eve.type == pygame.QUIT:
            Close()
        if eve.type == pygame.KEYDOWN:
            if eve.key == pygame.K_ESCAPE:
                Close()
    

    frame.fill(background)
    Bull(color , x , y , w)
    y +=y_change
    x +=x_change
    w += w_change
    #print(w)
    if w == 50:
        w_change = -1
    if w == 0:
        w_change = 1
    if y-55 < 0 and y_change == -1:
        y_change = 1
        color = (random.randrange(256) , random.randrange(256) , random.randrange(256))
    if y+55 > frame_y - 200 and y_change == 1:
        y_change = -1
        color = (random.randrange(256) , random.randrange(256) , random.randrange(256))
    if x+55 > frame_x - 200 and x_change == 1:
        x_change = -1
        color = (random.randrange(256) , random.randrange(256) , random.randrange(256))
    if x - 55 < 0 and x_change == -1:
        x_change = 1
        color = (random.randrange(256) , random.randrange(256) , random.randrange(256))
    
    clock.tick(60)
    pygame.display.update()
    
    