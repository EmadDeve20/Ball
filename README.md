Ball
=======

This program creates a ball that moves without the need for a controller. By tapping on the form wall, the direction changes. (Like Zero Player games) It's written with the Pygame library, and I'm not good at it. *If there is a problem with the code, I would be happy to report it to me :)*

step1
-----
install pygame library

```
pip install pygame
```
step2
-----
run the code

```
python Ball.py
```
